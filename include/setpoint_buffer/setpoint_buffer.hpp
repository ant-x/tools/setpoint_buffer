#ifndef SETPOINT_BUFFER_HPP
#define SETPOINT_BUFFER_HPP

#include "ros/ros.h"
#include "std_msgs/UInt8.h"
#include "mavros_msgs/PositionTarget.h"
#include "mavros_msgs/AttitudeTarget.h"
#include "mavros_antx/PoseTarget.h"
#include "mavros_antx/LMNFinjection.h"

class SetpointBuffer
{
public:
    SetpointBuffer();
    ~SetpointBuffer();

private:
    ros::NodeHandle _nh;

    // parameters
    std::string _position_target_input_topic;
    std::string _attitude_target_input_topic;
    std::string _pose_target_input_topic;
    std::string _lmnf_injection_input_topic;
    std::string _heartbeat_input_topic;
    std::string _position_target_output_topic;
    std::string _attitude_target_output_topic;
    std::string _pose_target_output_topic;
    double _publish_rate;
    double _idle_timeout;

    // subscribers
    ros::Subscriber _position_target_sub;
    ros::Subscriber _attitude_target_sub;
    ros::Subscriber _pose_target_sub;
    ros::Subscriber _lmnf_injection_sub;
    ros::Subscriber _heartbeat_sub;

    // publishers
    ros::Publisher _position_target_pub;
    ros::Publisher _attitude_target_pub;
    ros::Publisher _pose_target_pub;

    // input/output messages
    mavros_msgs::PositionTarget _position_target_input;
    mavros_msgs::AttitudeTarget _attitude_target_input;
    mavros_antx::PoseTarget _pose_target_input;

    // input message boolean
    bool _received_position_target{false};
    bool _received_attitude_target{false};
    bool _received_pose_target{false};
    bool _received_heartbeat{false};
    bool _send_idle{false};

    // time variables
    ros::Timer _timer;
    ros::Time _last_heartbeat_time;
    ros::Time _last_setpoint_time;
    static constexpr double MAX_HEARTBEAT_INTERVAL{2.0};    // max time interval between two heartbeat

    // message callbacks
    void positionTargetCallback(const mavros_msgs::PositionTarget::ConstPtr &msg);
    void attitudeTargetCallback(const mavros_msgs::AttitudeTarget::ConstPtr &msg);
    void poseTargetCallback(const mavros_antx::PoseTarget::ConstPtr & msg);
    void heartbeatCallback(const std_msgs::UInt8::ConstPtr &msg);
    void lmnfInjectionCallback(const mavros_antx::LMNFinjection::ConstPtr & msg);
    void mainLoop(const ros::TimerEvent &event);
};

#endif // SETPOINT_BUFFER_HPP
