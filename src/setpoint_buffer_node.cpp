#include "setpoint_buffer/setpoint_buffer.hpp"

SetpointBuffer::SetpointBuffer()
{
    _nh = ros::NodeHandle("~");

    _nh.param<std::string>("position_target_input_topic", _position_target_input_topic, "/position_target_input_topic");
    _nh.param<std::string>("attitude_target_input_topic", _attitude_target_input_topic, "/attitude_target_input_topic");
    _nh.param<std::string>("pose_target_input_topic", _pose_target_input_topic, "/pose_target_input_topic");
    _nh.param<std::string>("position_target_output_topic", _position_target_output_topic, "/position_target_output_topic");
    _nh.param<std::string>("attitude_target_output_topic", _attitude_target_output_topic, "/attitude_target_output_topic");
    _nh.param<std::string>("pose_target_output_topic", _pose_target_output_topic, "/pose_target_output_topic");
    _nh.param<std::string>("lmnf_injection_input_topic", _lmnf_injection_input_topic, "/lmnf_injection_input_topic");
    _nh.param<std::string>("heartbeat_input_topic", _heartbeat_input_topic, "/heartbeat_input_topic");
    _nh.param<double>("publish_rate", _publish_rate, 50.0);
    _nh.param<double>("idle_timeout", _idle_timeout, 20.0);

    _position_target_sub = _nh.subscribe(_position_target_input_topic, 1, &SetpointBuffer::positionTargetCallback, this);
    _attitude_target_sub = _nh.subscribe(_attitude_target_input_topic, 1, &SetpointBuffer::attitudeTargetCallback, this);
    _pose_target_sub = _nh.subscribe(_pose_target_input_topic, 1, &SetpointBuffer::poseTargetCallback, this);
    _lmnf_injection_sub = _nh.subscribe(_lmnf_injection_input_topic, 1, &SetpointBuffer::lmnfInjectionCallback, this);
    _heartbeat_sub = _nh.subscribe(_heartbeat_input_topic, 1, &SetpointBuffer::heartbeatCallback, this);

    _position_target_pub = _nh.advertise<mavros_msgs::PositionTarget>(_position_target_output_topic, 1);
    _attitude_target_pub = _nh.advertise<mavros_msgs::AttitudeTarget>(_attitude_target_output_topic, 1);
    _pose_target_pub = _nh.advertise<mavros_antx::PoseTarget>(_pose_target_output_topic, 1);

    _timer = _nh.createTimer(ros::Duration(1.0 / _publish_rate), &SetpointBuffer::mainLoop, this);
}

SetpointBuffer::~SetpointBuffer()
{
    
}

void SetpointBuffer::positionTargetCallback(const mavros_msgs::PositionTarget::ConstPtr &msg)
{
    // Handle Position Target message (https://gitlab.com/ant-x/tools/mavros/-/blob/antx/mavros_msgs/msg/PositionTarget.msg?ref_type=heads)
    _position_target_input = *msg;
    _last_setpoint_time = ros::Time::now();

    if ( _received_heartbeat  && !_received_position_target )
    {
        _received_position_target = true;
        _received_attitude_target = false;
        _received_pose_target = false;
        _send_idle = false;
        ROS_INFO("POS setpoint message received!");
    }
}

void SetpointBuffer::attitudeTargetCallback(const mavros_msgs::AttitudeTarget::ConstPtr &msg)
{
    // Handle Attitude Target message (https://gitlab.com/ant-x/tools/mavros/-/blob/antx/mavros_msgs/msg/AttitudeTarget.msg?ref_type=heads)
    _attitude_target_input = *msg;
    _last_setpoint_time = ros::Time::now();

    if ( _received_heartbeat && !_received_attitude_target )
    {
        _received_position_target = false;
        _received_attitude_target = true;
        _received_pose_target = false;
        _send_idle = false;
        ROS_INFO("ATT setpoint message received!");
    }
}

void SetpointBuffer::poseTargetCallback(const mavros_antx::PoseTarget::ConstPtr &msg)
{
    // Handle Pose Target message (https://gitlab.com/ant-x/tools/mavros/-/blob/antx/mavros_antx/msg/PoseTarget.msg?ref_type=heads)
    _pose_target_input = *msg;
    _last_setpoint_time = ros::Time::now();

    if ( _received_heartbeat && !_received_pose_target )
    {
        _received_position_target = false;
        _received_attitude_target = false;
        _received_pose_target = true;
        _send_idle = false;
        ROS_INFO("POSE setpoint message received!");
    }
}

void SetpointBuffer::lmnfInjectionCallback(const mavros_antx::LMNFinjection::ConstPtr &msg)
{
    // Handle LMNF Injection message (https://gitlab.com/ant-x/tools/mavros/-/blob/antx/mavros_antx/msg/LMNFinjection.msg?ref_type=heads)
    _last_setpoint_time = ros::Time::now();

    if ( _received_heartbeat )
    {
        ROS_INFO("LMNFinjection message received!");
    }
}

void SetpointBuffer::heartbeatCallback(const std_msgs::UInt8::ConstPtr &msg)
{
    // Handle uint8 standard message (https://docs.ros.org/en/electric/api/std_msgs/html/msg/UInt8.html)
    _last_heartbeat_time = ros::Time::now();

    if ( !_received_heartbeat )
    {
        _received_heartbeat = true;
        ROS_INFO("GCS link gained!");
    }
}

void SetpointBuffer::mainLoop(const ros::TimerEvent &event)
{
    if ( _received_heartbeat && ((ros::Time::now() - _last_heartbeat_time).toSec() > MAX_HEARTBEAT_INTERVAL) )
    { // IF the node is receiving the heartbeat message and the time between two consecutive heartbeats is greater than the MAX_HEARTBEAT_INTERVAL THAN stop sending any message
        _received_position_target = false;
        _received_attitude_target = false;
        _received_pose_target = false;
        _send_idle = false;
        _received_heartbeat = false;
        ROS_INFO("GCS link lost!");
    }

    if (_received_heartbeat && _idle_timeout > 0.0f && ((ros::Time::now() - _last_setpoint_time).toSec() > _idle_timeout))
    { // IF the node is receiving the heartbeat message and the time between two consecutive heartbeats is greater than the MAX_HEARTBEAT_INTERVAL THAN stop sending any message

        if (!_send_idle)
        {
            _received_position_target = false;
            _received_attitude_target = false;
            _received_pose_target = false;
            _send_idle = true;
            ROS_INFO("Sending idle target.");
        }
    }

    if (_received_heartbeat)
    {
        if (_received_position_target)
        {
            _position_target_input.header.stamp = ros::Time::now();
            _position_target_pub.publish(_position_target_input);
        }

        if (_received_attitude_target)
        {
            _attitude_target_input.header.stamp = ros::Time::now();
            _attitude_target_pub.publish(_attitude_target_input);
        }

        if (_received_pose_target)
        {
            _pose_target_input.header.stamp = ros::Time::now();
            _pose_target_pub.publish(_pose_target_input);
        }

        if (_send_idle)
        {
            mavros_msgs::AttitudeTarget _attitude_target_idle{};

            _attitude_target_idle.header.stamp = ros::Time::now();

            _attitude_target_idle.type_mask = 0;
            _attitude_target_idle.type_mask |= mavros_msgs::AttitudeTarget::IGNORE_ROLL_RATE;
            _attitude_target_idle.type_mask |= mavros_msgs::AttitudeTarget::IGNORE_PITCH_RATE;
            _attitude_target_idle.type_mask |= mavros_msgs::AttitudeTarget::IGNORE_YAW_RATE;

            _attitude_target_idle.orientation.x = 0.0f;
            _attitude_target_idle.orientation.y = 0.0f;
            _attitude_target_idle.orientation.z = 0.0f;
            _attitude_target_idle.orientation.w = 1.0f;

            _attitude_target_idle.body_rate.x = 0.0f;
            _attitude_target_idle.body_rate.y = 0.0f;
            _attitude_target_idle.body_rate.z = 0.0f;

            _attitude_target_idle.thrust = 0.0f;

            _attitude_target_pub.publish(_attitude_target_idle);
        }
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "setpoint_buffer_node");
    SetpointBuffer setpoint_buffer_node;
    ros::spin();

    return 0;
}
