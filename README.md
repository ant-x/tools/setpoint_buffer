# Setpoint Buffer

The ROS package `setpoint_buffer` is designed to subscribe to various low-frequency topics and then republish it on other topics at a higher frequency. 
It is useful when you want to adapt the data frequency between different components of a ROS system.

## Prerequisites

- ROS (Robot Operating System) - [ROS Installation](http://wiki.ros.org/ROS/Installation)
- Required ROS packages: `roscpp`, `std_msgs`, `mavros_msgs`, `mavros_antx`

## Installation

1. Make sure you have the prerequisites installed.
2. Clone the repository into your ROS workspace:

   ```bash
   cd /path/to/your/catkin_workspace/src
   git clone https://gitlab.com/ant-x/tools/setpoint_buffer.git
   ```
3. Build the package:
   ```bash
   cd /path/to/your/catkin_workspace
   catkin build   
   ```

## Configuration

The main parameters, which can be set as input arguments, are:

* `publish_rate`: Desired publishing frequency (default `50` Hz).
* `idle_timeout`: Timeout time since last setpoint received (default `20` s, set negative to disable idle feature).

Then, modify the `params/topic_names.yaml` file to customize topic names.

* `position_target_input_topic`: Topic name for input position data.
* `attitude_target_input_topic`: Topic name for input attitude data.
* `pose_target_input_topic`: Topic name for input pose data.
* `position_target_output_topic`: Topic name for output position data.
* `attitude_target_output_topic`: Topic name for output attitude data.
* `pose_target_output_topic`: Topic name for output pose data.
* `heartbeat_input_topic`: Topic name for heartbeat signal.

In order to allow the message forwarding a heartbeat message must be sent at a minimum frequency of `1 Hz`.

## Execution

1. Ensure your ROS workspace is in your `ROS_PACKAGE_PATH`:

   ```bash
   source /path/to/your/catkin_workspace/devel/setup.bash
   ```

2. Run the setpoint_buffer node:

   ```bash
   roslaunch setpoint_buffer setpoint_buffer.launch
   ```

   or, if you want to set `publish_rate` and `idle_timeout` with values different from the default

   ```bash
   roslaunch setpoint_buffer setpoint_buffer.launch publish_rate:=30 idle_timeout:=10
   ```

3. Check the published topics:

   ```bash
   rostopic list
   ```
   You should see new topics with names specified in the YAML parameters.

## Contributions

Contributions are welcome! For suggestions, improvements, or bug reports, refer to the "Issues" section on GitHub.

## License

This package is distributed under the [BSD-3-Clause License](LICENSE).

## Authors

- Mattia Giurato [mattia@antx.it](mailto:mattia@antx.it)
- Daniele Migliore [daniele@antx.it](mailto:daniele@antx.it)
- [OpenAI's ChatGPT](https://www.openai.com)
